# Print routines for nested dictionaries

def print_dict1(dic):
    for key in dic:
        if(type(dic[key]) == dict):
            print_dict1(dic[key])
        else:
            print(dic[key])


def print_dict2(dic):
    for key in dic:
        if(type(dic[key]) == dict):
            print("(Sub)Dict found, entering...")
            print_dict2(dic[key])
        else:
            print("\tSimple elem found: ", dic[key])
    print("(Sub)Dict done, leaving...")


def print_dict3(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "(Sub)Dict found, entering...")
            print_dict3(dic[key], indentlvl+1)
        else:
            print(indent + "Simple elem found: ", dic[key])


def print_dict4(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict4(dic[key], indentlvl+1)
        else:
            print(indent + "--->[element] " + str(dic[key]))


def print_dict5(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    arrow = ""
    if(indentlvl > 0):
        arrow = "--->"
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict5(dic[key], indentlvl+1)
        else:
            print(indent + arrow + "[element] " + str(dic[key]))


def print_dict6(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    arrow = ""
    if(indentlvl > 0):
        arrow = "--->"
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict6(dic[key], indentlvl+1)
        elif(type(dic[key]) == list):
            print(indent + "[list   ] " + str(key) + "\n" + indent*2 + "--->" + " " + str(dic[key]))
        else:
            print(indent + arrow + "[element] " + str(dic[key]))


def print_dict7(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    arrow = ""
    if(indentlvl > 0):
        arrow = "--->"
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict7(dic[key], indentlvl+1)
        elif(type(dic[key]) == list):
            print(indent + "[list   ] " + str(key) + " >>>> " + str(dic[key]))
        else:
            print(indent + arrow + "[element] " + str(key) + " >>>> " + str(dic[key]))


def main():
    d1 = {'bla': 1, 'boo': -123, 'baz': 3}
    d2 = {'erster': {'bla': 1, 'boo': -123, 'baz': 3}, 'zweiter': 3212}
    d3 = d2
    d3['dritter'] = [1,2,3,4]

    # print_dict1(d1)
    # print_dict2(d2)
    # print_dict3(d2,0)
    # print_dict4(d2,0)
    # print_dict5(d2,0)
    # print_dict6(d3,0)
    print_dict7(d3,0)


if(__name__ == "__main__"):
    main()