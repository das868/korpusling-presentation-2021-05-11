# A simple sample python parser for JSON

aufgabe = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123 \
}'


class Tree:
    id_counter = 0
    layer_counter = 0
    def __init__(self, layerno : int, name : str, parentid : int, subtree=None):
        if(subtree):
            self.subtree = subtree
        self.layerno = layerno
        self.name = name
        self.eigenid = Tree.id_counter
        Tree.id_counter += 1
        self.parentid = parentid
        print("\tTree created.")
    def print_all(self):
        print("(", self.layerno, ",'", self.name, "',", self.eigenid, ",", self.parentid, ")")
    def __str__(self):
        return ("(" + str(self.layerno) + ",'" + str(self.name) + "'," + str(self.eigenid) + "," + str(self.parentid) + ")")
    def add_leaf(self, leaf):
        print("\tLeaf added")



def parse_json(content):
    print(content)
    spl = content.split()
    print(spl)


def main():
    print("===================================================\n")
    #x = Tree(1,"bödödö",-1)
    #x.add_leaf("BLA")
    #x.print_all()
    ### Idee: Tree = [(Ebenen-No, 'name', eigenID, parentID)]
    exampletree = [(0,'bla',0,-1), (1,'foo',1,0), (1,'bar',2,0), (2,'baz',3,2)]
    #exampletree2 = [Tree(0,"bla",-1), Tree(1,"foo",0), Tree(1,"bar",0), Tree(2,"baz",2)]
    exampletree3 = [str(Tree(0,"bla",-1)), str(Tree(1,"foo",0)), str(Tree(1,"bar",0)), str(Tree(2,"baz",2))]
    print(exampletree3)
    print(exampletree)
    #print(map(str, exampletree2))
    #exampletree = exampletree.reverse()
    #print(sorted(exampletree, key=lambda a: a[2]))
    print("\n")
    print("===================================================\n")
    parse_json(aufgabe)


if(__name__ == "__main__"):
    main()
