# A simple sample python parser for JSON

aufgabe = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123, \
    "liste" : [1, 2, 3], \
    "testobject" : { \
        "num1" : 42 \
    } \
}'


class Tree:
    id_counter = 0
    layer_counter = 0
    def __init__(self, layerno : int, name : str, parentid : int, subtree=None):
        if(subtree):
            self.subtree = subtree
        self.layerno = layerno
        self.name = name
        self.eigenid = Tree.id_counter
        Tree.id_counter += 1
        self.parentid = parentid
        print("\tTree created.")
    def print_all(self):
        print("(", self.layerno, ",'", self.name, "',", self.eigenid, ",", self.parentid, ")")
    def __str__(self):
        return ("(" + str(self.layerno) + ",'" + str(self.name) + "'," + str(self.eigenid) + "," + str(self.parentid) + ")")
    def add_leaf(self, leaf):
        print("\tLeaf added")

# (layerno, name, eigenid, parentid)
class TreeElement:
    id_counter = 0
    layer_counter = 0
    def __init__(self, layerno : int, name : str, parentid : int, subtree=None):
        if(subtree):
            self.subtree = subtree
        self.layerno = layerno
        self.name = name
        self.eigenid = TreeElement.id_counter
        TreeElement.id_counter += 1
        self.parentid = parentid
        print("\tTreeElement created.")
    def print_all(self):
        print("(", self.layerno, ",'", self.name, "',", self.eigenid, ",", self.parentid, ")")
    def __str__(self):
        return ("(" + str(self.layerno) + ",'" + str(self.name) + "'," + str(self.eigenid) + "," + str(self.parentid) + ")")
    def add_leaf(self, leaf):
        print("\tLeaf added")


def parse_json(content):
    print(content)
    spl = content.split()
    print(spl)
    treeElemList = list()
    treeElemList.append(TreeElement(0,"root",-1))
    if(spl[0] != '{'):
        print("Error, { missing, not well formatted!")
    layerno = 0
    for elt in spl:
        print("--> Element: ", elt)
        if(elt == '{'):
            layerno += 1
        elif(elt == '}'):
            layerno -= 1
        else:
            treeElemList.append(TreeElement(layerno,elt,0))
            # treeElemList.append(elt)
        print("--> treeElemList: ")
        for x in treeElemList:
            print(x)
    
    #Find normal Key-Value-Pairs
    for i in range(0,len(treeElemList)):
        if(treeElemList[i].name == ':'):
            print("&&&&&&>> KeyValPair found: ", treeElemList[i-1].name, " : ", treeElemList[i+1].name)


def parse_json_func(json_str):
    # format: tuple(layerno : int, content : str, eigen_id : int, parent_id : int)
    layerno = 0
    eigen_id_counter = 0
    obj_list = list()
    obj_list.append((layerno, "root", eigen_id_counter, -1))
    eigen_id_counter += 1
    spl = json_str.split()
    print(spl, end="\n\n")
    for elt in spl:
        if(elt == '{'): # or elt[0] == '['):
            layerno += 1
        elif(elt == '}'): # or elt[-1] == ']'):
            layerno -= 1
        #elif(elt == '['):
        #    layerno += 1
        #elif(elt == ']'):
        #    layerno += 1
        else:
            if(elt[0] == '['):
                layerno += 1
            elif(elt[-1] == ']'):
                layerno -= 1
            
            if(layerno == 1):
                obj_list.append((layerno, elt, eigen_id_counter, 0))
            else:
                index = len(obj_list)-1
                for i in range(0,index):
                    if(obj_list[index-i][0] < layerno):
                        obj_list.append((layerno, elt, eigen_id_counter, obj_list[index-i][3]))
                        break
            eigen_id_counter += 1
    print(obj_list, end="\n\n")

    # Find key-value-pairs
    # for i in range(0,len(obj_list)):
    #     if(obj_list[i][1] == ':'):
    #         if(obj_list[i+1][1][0] == '['):
    #             print("==> List detected!")
    #             liststr = "" + obj_list[i+1][1]
    #             for j in range(i+2,len(obj_list)):
    #                 print("Checking elt: ", obj_list[j])
    #                 liststr += obj_list[j][1]
    #                 if(obj_list[j][1][-1] == ']'):
    #                     break
    #             print("==> List done: ", obj_list[i-1][1], " and ", liststr)
    #         else:
    #             print("-->Key-Val-Pair found on layer #", obj_list[i][0], ": ", obj_list[i-1][1], " and ", obj_list[i+1][1])

    # Create key-val dictionary
    keyvaldict = dict()
    for i in range(0,len(obj_list)):
        if(obj_list[i][1] == ':'):
            if(obj_list[i+1][1][0] == '['):
                print("==> List detected!")
                liststr = "" + obj_list[i+1][1]
                for j in range(i+2,len(obj_list)):
                    print("Checking elt: ", obj_list[j])
                    liststr += obj_list[j][1]
                    if(obj_list[j][1][-1] == ']'):
                        break
                print("==> List done: ", obj_list[i-1][1], " and ", liststr)
            elif(obj_list[i+1][1][0] == '{'):
                print("==> Object detected!")
                objstr = "" + obj_list[i+1][1]
                for j in range(i+2,len(obj_list)):
                    print("Checking elt: ", obj_list[j])
                    objstr += obj_list[j][1]
                    if(obj_list[j][1][-1] == '}'):
                        break
                print("==> Object done: ", obj_list[i-1][1], " and ", objstr)
            else:
                print("-->Key-Val-Pair found on layer #", obj_list[i][0], ": ", obj_list[i-1][1], " and ", obj_list[i+1][1])
                keyvaldict[obj_list[i-1][1]] = obj_list[i+1][1]
    print(keyvaldict)

    # Make dependency tree
    # deptree = list()
    # deptree.append(obj_list[0])
    # print("\nDeptree: ", deptree)

def main():
    #parse_json(aufgabe)
    print(('\n'+("=" * 80))*3, end="\n\n")
    parse_json_func(aufgabe)
    #print("root\n|\n+----elem1\n|\n+----elem2")


if(__name__ == "__main__"):
    main()
