# A simple sample python parser for JSON, re-thought entirely for wider input support

aufgabe1 = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123 \
}'

aufgabe2 = \
'{"name": "Mustermann", \
    "vorname":  "Max"\
}'

aufgabe3 = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123, \
    "liste" : [1, 2, 3], \
    "testobject" : { \
        "num1" : 42 \
    } \
}'

aufgabe4 = \
'{\
    "listebla":[1,2,3,4],\
    "listefoo":[9,8,7,6],\
    "objbar":{\
        "num":123,\
        "listebaz":[2,4,6,8,0]\
    }\
}'

aufgabe5 = \
'{\
    "listeobj":[{"num1":1,"num2":2,"liste2":[1,2,3,4]}]\
}'

aufgabe6 = \
'{\
    "num1" : -123,\
    "bool1" : false,\
    "list1" : [1, "hi", true],\
    "str1" : "Max Mustermann",\
    "obj2" : {\
        "str2" : "Nested Object!",\
        "num2" : 42\
    }\
}'

aufgabe7 = \
'{\
    "num1" : -123,\
    "num2" : -123.45678,\
    "bool1" : false,\
    "bool2" : true,\
    "num3" : -0.020202022\
}'

aufgabe8 = \
'{\
    "num1" : -123,\
    "num2" : -123.456789,\
    "bool1" : false,\
    "bool2" : true,\
    "string1" : "Max Muster,mann",\
    "num3" : -0.020202022\
}'


def trim_json(json_str):
    resultstr = ""
    for i in range(0,len(json_str)):
        if(json_str[i] == '\t' or json_str[i] == '\n'):
            continue
        elif(json_str[i] == ' '):
            #schauen, ob "" außenrum stehen, sonst weg damit!
            # d.h. schauen, ob ungerade Anzahl an " davor stehen, d.h. eine offen ist!
            counter = 0
            for j in range(0,i):
                if(json_str[j] == '"'):
                    counter += 1
            if(counter % 2 == 1):
                resultstr += json_str[i]
            else:
                continue
        else:
            resultstr += json_str[i]
    return resultstr


def parse_json_bool(workwith : str, strpointer : int, resultdict : dict, keystr : str): #workwith=trimmed_json_string, strpointer = whereami, keystr=parsed_key
    if(workwith[strpointer:strpointer+4] == 'true'):
        print("\tBoolean value extracted: true")
        strpointer += 4
        print("\tNEW ww[strptr]=", workwith[strpointer])
        resultdict[keystr] = True
    elif(workwith[strpointer:strpointer+5] == 'false'):
        print("\tBoolean value extracted: false")
        strpointer += 5
        # print("\tNEW ww[strptr]=", workwith[strpointer])
        resultdict[keystr] = False
    return (resultdict, strpointer)


def parse_json_num(workwith : str, strpointer : int, resultdict : dict, keystr : str): #workwith=trimmed_json_string, strpointer = whereami, keystr=parsed_key
    if(workwith[strpointer] in "-0123456789"):
        print("Trying to parse num...")
        is_int = True
        is_double = True
        res = 0
        tok = 0
        print("ww[...]: ", workwith[strpointer:-1])
        for k in range(strpointer+1,len(workwith)):
            tok = k
            print("Entered loop, content: ", workwith[strpointer:k])
            if(workwith[k] == ','):
                tok = k
                print("FINAL K: ", k, " in tok: ", tok)
                break
            if(workwith[k] in "]}"):
                tok = k
                print("FINAL K: ", k, " in tok: ", tok)
                break
            if(not(workwith[k] in "0123456789")):
                is_int &= False
                if(workwith[k] == '.' and workwith[k+1] in "0123456789"):
                    continue
                else:
                    is_double &= False
                    print("NUM parse ERROR: neither int nor float...")
                    break
        print("\tABOUT TO PARSE NUM: ", workwith[strpointer:tok], "tok=", tok)
        if(is_int):
            res = int(workwith[strpointer:tok])
        elif(is_double):
            res = float(workwith[strpointer:tok])
        resultdict[keystr] = res
        strpointer += len(str(res))+1
    return (resultdict, strpointer)


def parse_json_string(workwith : str, strpointer : int, resultdict : dict, keystr : str): #workwith=trimmed_json_string, strpointer = whereami, keystr=parsed_key
    if(workwith[strpointer] == '"'):
        valstr = ""
        for l in range(strpointer+1,len(workwith)-1): #look for corresponding '"' and treat the inbetween as value
            if(workwith[l] == '"'):
                break
            valstr += workwith[l]
            l += 1
        print("String value found:\t", valstr)
        resultdict[keystr] = valstr
        strpointer += len(valstr)+2 # +1 for '"' and +1 to get to next char
    return (resultdict, strpointer)


def parse_json_list(workwith : str, strpointer : int, resultdict : dict, keystr : str): #workwith=trimmed_json_string, strpointer = whereami, keystr=parsed_key
    return (resultdict, strpointer)


def parse_json_object(workwith : str, strpointer : int, resultdict : dict, keystr : str): #workwith=trimmed_json_string, strpointer = whereami, keystr=parsed_key
    return (resultdict, strpointer)


def parse_json(json_str : str):
    workwith = trim_json(json_str)
    resultdict = dict()
    print("workwith: ", workwith, "\tlen(ww): ", len(workwith), "ww[43]: ", workwith[43:58])
    if(workwith[0] == '{' and workwith[1] == '"' and workwith[-1] == '}'):
        strpointer = 1
        while(strpointer < (len(workwith)-1)): #limits to exclude outer '{'...'}'
            print("======== strptr = ", strpointer, "resultdict: ", resultdict)
            if(workwith[strpointer] == '"'): #key-val pair incoming: key detected
                print("Trying to parse key")
                
                # parse key, always a string
                keystr = ""
                toj = 0
                for j in range(strpointer+1,len(workwith)-1): #look for corresponding '"' and treat the inbetween as key
                    #print("workwith[j] = ", workwith[j])
                    if(workwith[j] == '"'):
                        toj = j
                        break
                    keystr += workwith[j]
                    j += 1
                print("Key extracted: ", keystr)
                if(workwith[toj+1] != ':'):
                    print("ERROR! Expected ':' after key...")
                    return resultdict
                strpointer=toj+2 #end of str-key + ':', onto next char
                
                # parse value, could be of type bool/num/str/list/object=dict
                print("strptr: ", strpointer, "\tww[strptr]=", workwith[strpointer])
                parsed = (dict(),0)
                # Was steht nach dem Doppelpunkt?
                # --> BOOL?
                if(workwith[strpointer:strpointer+4] == 'true' or workwith[strpointer:strpointer+5] == 'false'):
                    parsed = parse_json_bool(workwith, strpointer, resultdict, keystr)
                    print("\n\tPARSED: ", parsed, "\n")
                    resultdict = parsed[0]
                    strpointer = parsed[1]
                # if(workwith[strpointer:strpointer+4] == 'true'):
                #     print("\tBoolean value extracted: true")
                #     strpointer += 4
                #     print("\tNEW ww[strptr]=", workwith[strpointer])
                #     resultdict[keystr] = True
                # elif(workwith[strpointer:strpointer+5] == 'false'):
                #     print("\tBoolean value extracted: false")
                #     strpointer += 5
                #     print("\tNEW ww[strptr]=", workwith[strpointer])
                #     resultdict[keystr] = False
                # --> ZAHL?
                elif(workwith[strpointer] in "-0123456789"):
                    parsed = parse_json_num(workwith, strpointer, resultdict, keystr)
                    print("\n\tPARSED: ", parsed, "\n")
                    resultdict = parsed[0]
                    strpointer = parsed[1]
                # elif(workwith[strpointer] in "-0123456789"):
                #     print("Trying to parse num...")
                #     is_int = True
                #     is_double = True
                #     res = 0
                #     tok = 0
                #     print("ww[...]: ", workwith[strpointer:-1])
                #     for k in range(strpointer+1,len(workwith)):
                #         tok = k
                #         print("Entered loop, content: ", workwith[strpointer:k])
                #         if(workwith[k] == ','):
                #             tok = k
                #             print("FINAL K: ", k, " in tok: ", tok)
                #             break
                #         if(workwith[k] in "]}"):
                #             tok = k
                #             print("FINAL K: ", k, " in tok: ", tok)
                #             break
                #         if(not(workwith[k] in "0123456789")):
                #             is_int &= False
                #             if(workwith[k] == '.' and workwith[k+1] in "0123456789"):
                #                 continue
                #             else:
                #                 is_double &= False
                #                 print("NUM parse ERROR: neither int nor float...")
                #                 break
                #     print("\tABOUT TO PARSE NUM: ", workwith[strpointer:tok], "tok=", tok)
                #     if(is_int):
                #         res = int(workwith[strpointer:tok])
                #     elif(is_double):
                #         res = float(workwith[strpointer:tok])
                #     resultdict[keystr] = res
                #     strpointer += len(str(res))+1
                    # print("Next CHAR: ", workwith[strpointer])
                # --> STRING?
                #if(False): print("doooooooooooooof")
                elif(workwith[strpointer] == '"'):
                    parsed = parse_json_string(workwith, strpointer, resultdict, keystr)
                    print("\n\tPARSED: ", parsed, "\n")
                    resultdict = parsed[0]
                    strpointer = parsed[1]
                    # valstr = ""
                    # for l in range(strpointer+1,len(workwith)-1): #look for corresponding '"' and treat the inbetween as value
                    #     if(workwith[l] == '"'):
                    #         break
                    #     valstr += workwith[l]
                    #     l += 1
                    # print("String value found:\t", valstr)
                    # resultdict[keystr] = valstr
                    # strpointer += len(valstr)+2 # +1 for '"' and +1 to get to next char
                # --> LIST?
                # elif(workwith[strpointer] == '['):
                #     vallist = list()
                #     tom = 0
                # break #debug!
            else:
                strpointer += 1 #for debug only? necessary for last key-val-pair? -> no comma...
    else:
        print("ERROR! JSON doesn't start and end w/ '{' / '}'")
        return resultdict
    return resultdict



def print_dict7(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    arrow = ""
    if(indentlvl > 0):
        arrow = "--->"
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict7(dic[key], indentlvl+1)
        elif(type(dic[key]) == list):
            print(indent + "[list   ] " + str(key) + " >>>> " + str(dic[key]))
        else:
            print(indent + arrow + "[element] " + str(key) + " >>>> " + str(dic[key]))


def main():
    print(parse_json(aufgabe8))
    # print(parse_json_bool('false', 0, dict(), "bool1"))
    # print(parse_json_num('-123.45678901234,', 0, dict(), "num1")) #num is parse until comma!


if(__name__ == "__main__"):
    main()