# A simple sample python parser for JSON, re-thought entirely for wider input support

aufgabe1 = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123 \
}'

aufgabe2 = \
'{"name": "Mustermann", \
    "vorname":  "Max"\
}'

aufgabe3 = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123, \
    "liste" : [1, 2, 3], \
    "testobject" : { \
        "num1" : 42 \
    } \
}'

aufgabe4 = \
'{\
    "listebla":[1,2,3,4],\
    "listefoo":[9,8,7,6],\
    "objbar":{\
        "num":123,\
        "listebaz":[2,4,6,8,0]\
    }\
}'

aufgabe5 = \
'{\
    "listeobj":[{"num1":1,"num2":2,"liste2":[1,2,3,4]}]\
}'

aufgabe6 = \
'{\
    "num1" : -123,\
    "bool1" : false,\
    "list1" : [1, "hi", true],\
    "str1" : "Max Mustermann",\
    "obj2" : {\
        "str2" : "Nested Object!",\
        "num2" : 42\
    }\
}'


def trim_json(json_str):
    resultstr = ""
    for i in range(0,len(json_str)):
        if(json_str[i] == '\t' or json_str[i] == '\n'):
            continue
        elif(json_str[i] == ' '):
            #schauen, ob "" außenrum stehen, sonst weg damit!
            # d.h. schauen, ob ungerade Anzahl an " davor stehen, d.h. eine offen ist!
            counter = 0
            for j in range(0,i):
                if(json_str[j] == '"'):
                    counter += 1
            if(counter % 2 == 1):
                resultstr += json_str[i]
            else:
                continue
        else:
            resultstr += json_str[i]
    return resultstr


def parse_json(json_str, obj_counter : int, list_counter : int):
    workwith = trim_json(json_str)
    resultdict = dict()
    # print(workwith)
    # return resultdict
    i = 0
    # obj_counter = list_counter = 0
    stop = len(workwith)
    while(i<stop):
        if(workwith[i] == '{'):
            #object, also schau, wos endet (bei nächster "}"), call Rekursion & add zu dict
            j = stop-1
            while(j>0):
                if(workwith[j] == '}'):
                    # print("Suche..., j=", j, "index=", i, (stop-j))
                    dictstring = "object" + str(obj_counter)
                    obj_counter += 1
                    # print("OBJECT: trimming: ", workwith[i+1:j], "\n", trim_json(workwith[i+1:j]))
                    resultdict[dictstring] = parse_json(workwith[i+1 : j], obj_counter, list_counter)
                    i=j
                    break
                j-=1
        elif(workwith[i] == '['):
            #list, also schau, wos endet, füg alles dazw in Liste und das in dict ein
            for j in range(i+1,stop):
                if(workwith[j] == ']'):
                    dictstring = "list" + str(list_counter)
                    list_counter += 1
                    helplist = []
                    for k in range(i+1,j):
                        helplist.append(workwith[k])
                    resultdict[dictstring] = helplist
                    i+=1
                    break
        i+=1
    return resultdict


def parse_json_Sich1(json_str, obj_counter : int, list_counter : int):
    workwith = trim_json(json_str)
    resultdict = dict()
    i = 0
    # obj_counter = list_counter = 0
    stop = len(workwith)
    while(i<stop):
        if(workwith[i] == '{'):
            #object, also schau, wos endet (von hinten!), call Rekursion & add zu dict
            j = stop-1
            while(j>0):
                if(workwith[j] == '}'):
                    # print("Suche..., j=", j, "index=", i, (stop-j))
                    dictstring = "object" + str(obj_counter)
                    obj_counter += 1
                    # print("OBJECT: trimming: ", workwith[i+1:j], "\n", trim_json(workwith[i+1:j]))
                    resultdict[dictstring] = parse_json_Sich1(workwith[i+1 : j], obj_counter, list_counter)
                    i=j
                    break
                j-=1
        elif(workwith[i] == '['):
            #list, also schau, wos endet, füg alles dazw in Liste und das in dict ein
            for j in range(i+1,stop):
                if(workwith[j] == ']'):
                    dictstring = "list" + str(list_counter)
                    list_counter += 1
                    helplist = []
                    for k in range(i+1,j):
                        helplist.append(workwith[k])
                    resultdict[dictstring] = helplist
                    i+=1
                    break
        i+=1
    return resultdict


def parse_json_lists_objs(json_str, obj_counter : int, list_counter : int):
    workwith = trim_json(json_str)
    resultdict = dict()
    i = 0
    # obj_counter = list_counter = 0
    stop = len(workwith)
    while(i<stop):
        if(workwith[i] == '{'):
            #object, also schau, wos endet (von hinten!), call Rekursion & add zu dict
            j = stop-1
            while(j>0):
                if(workwith[j] == '}'):
                    # print("Suche..., j=", j, "index=", i, (stop-j))
                    dictstring = "object" + str(obj_counter)
                    obj_counter += 1
                    # print("OBJECT: trimming: ", workwith[i+1:j], "\n", trim_json(workwith[i+1:j]))
                    resultdict[dictstring] = parse_json_lists_objs(workwith[i+1 : j], obj_counter, list_counter)
                    i=j
                    break
                j -= 1
        elif(workwith[i] == '['):
            #list, also schau, wos endet, füg alles dazw in Liste und das in dict ein
            for j in range(i+1,stop):
                if(workwith[j] == ']'):
                    dictstring = "list" + str(list_counter)
                    list_counter += 1
                    helplist = []
                    for k in range(i+1,j):
                        helplist.append(workwith[k])
                    resultdict[dictstring] = helplist
                    i+=1
                    break
        i+=1
    return resultdict


def parse_json_neu(json_str):
    workwith = trim_json(json_str)
    resultdict = dict()
    i = 0
    obj_counter = list_counter = 0
    stop = len(workwith)
    while(i<stop):
        if(workwith[i] == '{'):
            #object, also schau, wos endet (von hinten!), call Rekursion & add zu dict
            j = stop-1
            while(j>0):
                if(workwith[j] == '}'):
                    # print("Suche..., j=", j, "index=", i, (stop-j))
                    dictstring = "object" + str(obj_counter)
                    obj_counter += 1
                    # print("OBJECT: trimming: ", workwith[i+1:j], "\n", trim_json(workwith[i+1:j]))
                    resultdict[dictstring] = parse_json_neu(workwith[i+1 : j])
                    i=j
                    break
                j -= 1
        elif(workwith[i] == '['):
            #list, also schau, wos endet, füg alles dazw in Liste und das in dict ein
            for j in range(i+1,stop):
                if(workwith[j] == ']'):
                    dictstring = "list" + str(list_counter)
                    list_counter += 1
                    helplist = []
                    for k in range(i+1,j):
                        helplist.append(workwith[k])
                    resultdict[dictstring] = helplist
                    i+=1
                    break
        i+=1
    return resultdict


def parse_json_alt(json_str):
    workwith = trim_json(json_str)
    resultdict = dict()
    i = 0
    obj_counter = list_counter = 0
    stop = len(workwith)
    while(i<stop):
        if(workwith[i] == '{'):
            #object, also schau, wos endet (von hinten!), call Rekursion & add zu dict
            for j in range(i+1,stop):
                if(workwith[j] == '}'):
                    dictstring = "object" + str(obj_counter)
                    obj_counter += 1
                    # print("OBJECT: trimming: ", workwith[i+1:j], "\n", trim_json(workwith[i+1:j]))
                    resultdict[dictstring] = parse_json_alt(workwith[i+1 : j])
                    i=j
                    break
        elif(workwith[i] == '['):
            #list, also schau, wos endet, füg alles dazw in Liste und das in dict ein
            for j in range(i+1,stop):
                if(workwith[j] == ']'):
                    dictstring = "list" + str(list_counter)
                    list_counter += 1
                    helplist = []
                    for k in range(i+1,j):
                        helplist.append(workwith[k])
                    resultdict[dictstring] = helplist
                    i+=1
                    break
        i+=1
    return resultdict


def print_dict7(dic : dict, indentlvl : int):
    indent = "  " * indentlvl
    arrow = ""
    if(indentlvl > 0):
        arrow = "--->"
    for key in dic:
        if(type(dic[key]) == dict):
            print(indent + "[object ] " + str(key) + "")
            print_dict7(dic[key], indentlvl+1)
        elif(type(dic[key]) == list):
            print(indent + "[list   ] " + str(key) + " >>>> " + str(dic[key]))
        else:
            print(indent + arrow + "[element] " + str(key) + " >>>> " + str(dic[key]))


def main():
    # trimmed1 = trim_json(aufgabe1)
    # print(trimmed1)
    # trimmed2 = trim_json(aufgabe2)
    # print(trimmed2)
    # trimmed3 = trim_json(aufgabe3)
    # print(trimmed3)
    # print("\n")
    # print(aufgabe1, aufgabe2, aufgabe3, sep='\n')
    #print(parse_json(aufgabe4))

    # print_dict7(parse_json(aufgabe4,0,0),0)
    print_dict7(parse_json(aufgabe6,0,0),0)


if(__name__ == "__main__"):
    main()