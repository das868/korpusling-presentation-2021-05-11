# A simple sample python parser for JSON

aufgabe = \
'{ \
    "name" : "Mustermann", \
    "vorname" : "Max", \
    "alter" : 123, \
    "liste" : [1, 2, 3], \
    "testobject" : { \
        "num1" : 42 \
    } \
}'


def parse_json2(json_str):
    pass


def parse_json(json_str):
    # format: tuple(layerno : int, content : str, eigen_id : int, parent_id : int)
    layerno = 0
    eigen_id_counter = 0
    obj_list = list()
    obj_list.append((layerno, "root", eigen_id_counter, -1))
    eigen_id_counter += 1
    spl = json_str.split()
    print(spl, end="\n\n")
    for elt in spl:
        if(elt == '{'): # or elt[0] == '['):
            layerno += 1
            obj_list.append((layerno, elt, eigen_id_counter, -99))
        elif(elt == '}'): # or elt[-1] == ']'):
            layerno -= 1
            obj_list.append((layerno, elt, eigen_id_counter, -99))
        #elif(elt == '['):
        #    layerno += 1
        #elif(elt == ']'):
        #    layerno += 1
        else:
            if(elt[0] == '['):
                layerno += 1
            elif(elt[-1] == ']'):
                layerno -= 1
            
            if(layerno == 1):
                obj_list.append((layerno, elt, eigen_id_counter, 0))
            else:
                index = len(obj_list)-1
                for i in range(0,index):
                    if(obj_list[index-i][0] < layerno):
                        obj_list.append((layerno, elt, eigen_id_counter, obj_list[index-i][3]))
                        break
            eigen_id_counter += 1
    print(obj_list, end="\n\n")

    # Find key-value-pairs
    # for i in range(0,len(obj_list)):
    #     if(obj_list[i][1] == ':'):
    #         if(obj_list[i+1][1][0] == '['):
    #             print("==> List detected!")
    #             liststr = "" + obj_list[i+1][1]
    #             for j in range(i+2,len(obj_list)):
    #                 print("Checking elt: ", obj_list[j])
    #                 liststr += obj_list[j][1]
    #                 if(obj_list[j][1][-1] == ']'):
    #                     break
    #             print("==> List done: ", obj_list[i-1][1], " and ", liststr)
    #         else:
    #             print("-->Key-Val-Pair found on layer #", obj_list[i][0], ": ", obj_list[i-1][1], " and ", obj_list[i+1][1])

    # Create key-val dictionary
    keyvaldict = dict()
    for i in range(0,len(obj_list)):
        if(obj_list[i][1] == ':'):
            if(obj_list[i+1][1][0] == '['):
                print("==> List detected!")
                liststr = "" + obj_list[i+1][1]
                for j in range(i+2,len(obj_list)):
                    print("\tChecking elt: ", obj_list[j])
                    liststr += obj_list[j][1]
                    if(obj_list[j][1][-1] == ']'):
                        break
                print("==> List done: ", obj_list[i-1][1], " and ", liststr)
                keyvaldict[obj_list[i-1][1]] = liststr
            elif(obj_list[i+1][1] == '{'):
                print("==> Object detected!")
                objstr = "" + obj_list[i+1][1]
                for j in range(i+2,len(obj_list)):
                    print("\tChecking elt: ", obj_list[j])
                    objstr += obj_list[j][1]
                    if(obj_list[j][1] == '}'):
                        break
                print("==> Object done: ", obj_list[i-1][1], " and ", objstr)
                keyvaldict[obj_list[i-1][1]] = parse_json('{ "num1" : 42 }')
            else:
                print("-->Key-Val-Pair found on layer #", obj_list[i][0], ": ", obj_list[i-1][1], " and ", obj_list[i+1][1])
                keyvaldict[obj_list[i-1][1]] = obj_list[i+1][1]
    print(keyvaldict)
    return keyvaldict

    # Make dependency tree
    # deptree = list()
    # deptree.append(obj_list[0])
    # print("\nDeptree: ", deptree)


def print_dict(dictionary):
    for key in dictionary:
        if(type(dictionary[key]) == dict):
            print(key, " says SUBDICT:")
            print_dict(dictionary[key])
        else:
            print(key, " says ", dictionary[key])


def main():
    #parse_json(aufgabe)
    print(('\n'+("=" * 80))*3, end="\n\n")
    stub = parse_json(aufgabe)
    print_dict(stub)
    #parse_json2(aufgabe)
    #print("root\n|\n+----elem1\n|\n+----elem2")


if(__name__ == "__main__"):
    main()
