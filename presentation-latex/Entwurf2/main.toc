\beamer@sectionintoc {1}{Grundideen von Datenstrukturen}{2}{0}{1}
\beamer@sectionintoc {2}{JSON}{10}{0}{2}
\beamer@subsectionintoc {2}{1}{Theorie/Syntax}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Anwendungsaufgaben}{12}{0}{2}
\beamer@sectionintoc {3}{XML}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Theorie/Syntax}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Anwendungsaufgaben}{15}{0}{3}
\beamer@sectionintoc {4}{Vergleich JSON--XML}{16}{0}{4}
\beamer@sectionintoc {5}{TEI}{18}{0}{5}
\beamer@subsectionintoc {5}{1}{Idee}{19}{0}{5}
\beamer@subsectionintoc {5}{2}{Die wichtigsten Tags}{20}{0}{5}
\beamer@subsectionintoc {5}{3}{Anwendungsaufgaben}{21}{0}{5}
\babel@toc {ngerman}{}
