\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Grundideen von Datenstrukturen}{5}{0}{1}
\beamer@sectionintoc {2}{JSON}{32}{0}{2}
\beamer@subsectionintoc {2}{1}{Theorie/Syntax}{34}{0}{2}
\beamer@subsectionintoc {2}{2}{Anwendungsaufgaben}{49}{0}{2}
\beamer@sectionintoc {3}{XML}{53}{0}{3}
\beamer@subsectionintoc {3}{1}{Theorie/Syntax}{58}{0}{3}
\beamer@subsectionintoc {3}{2}{Anwendungsaufgaben}{69}{0}{3}
\beamer@sectionintoc {4}{Vergleich JSON--XML}{71}{0}{4}
\beamer@sectionintoc {5}{TEI}{73}{0}{5}
\beamer@subsectionintoc {5}{1}{Idee}{73}{0}{5}
\beamer@subsectionintoc {5}{2}{Die wichtigsten Tags}{78}{0}{5}
\beamer@subsectionintoc {5}{3}{Anwendungsaufgaben}{80}{0}{5}
