\beamer@sectionintoc {1}{Ziele von Datenstrukturen}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Alltagsbeispiele}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Demonstration Nutzen}{4}{0}{1}
\beamer@sectionintoc {2}{JSON}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Theorie/Syntax}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Anwendungsaufgabe}{7}{0}{2}
\beamer@sectionintoc {3}{XML}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Theorie/Syntax}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Anwendungsaufgabe}{10}{0}{3}
\beamer@sectionintoc {4}{Vergleich JSON--XML}{11}{0}{4}
\beamer@sectionintoc {5}{TEI}{13}{0}{5}
\beamer@subsectionintoc {5}{1}{Theorie}{14}{0}{5}
\beamer@subsectionintoc {5}{2}{Die wichtigsten Tags}{15}{0}{5}
\beamer@subsectionintoc {5}{3}{Beispiel???}{16}{0}{5}
\beamer@subsectionintoc {5}{4}{Anwendungsaufgabe}{17}{0}{5}
\beamer@sectionintoc {6}{Nützliche Hilfsmittel}{18}{0}{6}
\babel@toc {ngerman}{}
